package shape;

public abstract class Shape {

    private double area;

    public abstract double getArea();

}

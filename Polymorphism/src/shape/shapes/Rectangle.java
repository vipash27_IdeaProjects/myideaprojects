package shape.shapes;

import shape.Shape;

public class Rectangle extends Shape {

    private double length;
    private double width;

    public Rectangle() {

    }

    public Rectangle(double l, double w) {
        length = l;
        width = w;
    }

    @Override
    public double getArea() {
        return length * width;
    }
}

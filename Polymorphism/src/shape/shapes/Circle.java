package shape.shapes;

import shape.Shape;

public class Circle extends Shape {

    private double radius;

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return 3.14 * (radius * radius);
    }

}

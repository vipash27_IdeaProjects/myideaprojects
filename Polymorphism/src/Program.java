import shape.Shape;
import shape.shapes.Circle;
import shape.shapes.Rectangle;

import java.util.Stack;

public class Program {
    public static void main(String[] args) {

        Shape circle = new Circle(10);
        System.out.println("Area of circle: " + circle.getArea());

        Shape rectangle = new Rectangle(10, 5);
        System.out.println("Area of rectangle: " + rectangle.getArea());

        Stack stack = new Stack();
        stack.push(circle);
        stack.push(rectangle);
        System.out.println(stack.size());

        while (!stack.empty()) {
            Shape shape = (Shape) stack.pop();
            System.out.println("Area of shape: " + shape.getArea());
        }

        System.out.println(stack.size());
    }
}

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class MyScanner {

    private Scanner scanner;
    private int num;
    private String name;

    public String getName() {
        scanner = new Scanner(System.in);
        System.out.print("Input a name of type file: .");
        name = scanner.nextLine();
        return name;
    }

    public int getNumberSearch(int arrayLength) {
        while (true) {
            scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                num = scanner.nextInt();
                if (num > 0 && num <= arrayLength + 1)
                break;
                else {
                    System.out.print("Incorrect input! Try again.\nEnter a number for search: ");
                }
            }
            else {
                System.out.print("Incorrect input! Try again.\nEnter a number for search: ");
            }
        }
        return num;
    }

    public File getFileForStart(List<File> fileList) {
        while (true) {
            System.out.print("Input a number of file for start: ");
            scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                num = scanner.nextInt();
                if (num > 0 && num <= fileList.size()) {
                    return fileList.get(num - 1);
                }
                else {
                    System.out.print("Incorrect input! Try again.\nInput a number of file for start: ");
                }
            }
            else {
                System.out.print("Incorrect input! Try again.\nInput a number of file for start: ");
            }
        }
    }
}

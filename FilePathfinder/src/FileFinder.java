import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileFinder {

    private List<File> list;

    public List<File> getList(File[] roots, String name, int index) {
        list = new ArrayList<>();
        if (index == roots.length + 1) {
            System.out.println(name);
        }
        else {
            System.out.println("Find in " + roots[index - 1] + "... Please wait!");
            getAllFiles(roots[index - 1]);
        }
        getFilesOfNessarryType(name);
        return list;
    }

    private void getAllFiles(File root) {
        File[] files = root.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    getAllFiles(files[i]);
                }
                else {
                    list.add(files[i]);
                }
            }
        }
    }

    private void getFilesOfNessarryType(String name) {
        List<File> fileList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            int begin = name.length();
            int end = list.get(i).getName().length();
            if(list.get(i).getName().substring(end - begin).equals(name)) {
                fileList.add(list.get(i));
                Desktop desktop = null;
                if (Desktop.isDesktopSupported()) {
                    desktop = Desktop.getDesktop();
                }
                try {
                    desktop.open(new File(String.valueOf(list.get(i))));
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        list = fileList;
    }
}

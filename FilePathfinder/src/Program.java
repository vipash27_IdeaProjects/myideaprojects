import java.io.File;
import java.util.List;

public class Program {

    private static File[] roots;

    public static void main(String[] args) {

        RootDisk rootDisk = new RootDisk();
        roots = rootDisk.getRoots();

        MyScanner scanner = new MyScanner();
        int index = scanner.getNumberSearch(roots.length);
        String name = scanner.getName();

        FileFinder fileFinder = new FileFinder();
        List<File> fileList = fileFinder.getList(roots, name, index);

        for (int i = 0; i < fileList.size(); i++) {
            System.out.println((i + 1) + ". " + fileList.get(i).getAbsolutePath());
        }

        System.out.println("====================================");
        File file = scanner.getFileForStart(fileList);
        
    }
}

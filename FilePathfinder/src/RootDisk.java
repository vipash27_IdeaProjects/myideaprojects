import java.io.File;

public class RootDisk {

    private File[] roots;

    public File[] getRoots() {
        roots = File.listRoots();
        System.out.println("There are " + roots.length + " disk:");
        for (int i = 0; i < roots.length; i++) {
            System.out.println((i + 1) + ". " + roots[i].getPath());
        }
        System.out.println((roots.length + 1) + ". All");
        System.out.print("Enter a number for search: ");
        return roots;
    }
}

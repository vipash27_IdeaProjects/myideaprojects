import java.util.Scanner;

public class Program {

    private static Scanner scanner;
    private static double result;

    public static void main(String[] args) {
        calculate();
        System.out.println("The result is: " + result);
    }

    private static void calculate() {
        result = setData();
        while(true) {
            String str = setOperation();
            if (str.equals("=")) {
                break;
            }
            else if (str.equals("+")) {
                result += setData();
            }
            else if (str.equals("-")) {
                result -= setData();
            }
            else if (str.equals("*")) {
                result *= setData();
            }
            else if (str.equals("/")) {
                result /= setData();
            }

        }
    }

    private static double setData() {
        double x = 0;
        while (true) {
            scanner = new Scanner(System.in);
            System.out.print("Input a value: ");
            if (scanner.hasNextDouble()) {
                x = scanner.nextDouble();
                break;
            }
            else {
                System.out.print("Incorrect data! ");
            }
        }
        return x;
    }

    private static String setOperation() {
        String str;
        while (true) {
            System.out.print("Input operation: ");
            scanner = new Scanner(System.in);
            str = scanner.nextLine();
            if (str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/") || str.equals("=")) {
                break;
            }
            else {
                System.out.println("Incorrect data!");
            }
        }
        return str;
    }
}

public class HelloArray {
    public static void main(String[] args) {
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10);
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        /*
        // Сортировка пузырьком / Bubble sort
        arr = getBubbleSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
         */

        /*
        // Shaker sort
        arr = getSortedArrayUsingShakerSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
         */

        /*System.out.println("==================================");
        // task 1: получить максимальное значение из массива
        int maxValueFromArray = getMaxValueFromArray(arr);
        System.out.println(maxValueFromArray);
        System.out.println("==================================");
        // task2: создать новый массив данных, размер и значения которого задаются из массива arr, при этом значения в вновь созданном массиве не доллжны превышать значения равное 3
        int[] arr1 = getArrayWithLowScores(arr);
        for (int i = 0; i < arr1.length; i++) {
            System.out.println(arr1[i]);
        }*/
    }

    private static int getMaxValueFromArray(int[] arr) {
        int maxValue = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (maxValue < arr[i]) {
                maxValue = arr[i];
            }
        }
        return maxValue;
    }

    // метод, с помощью которого получаем новый массив, где value of each element <= 3
    private static int[] getArrayWithLowScores(int[] arr) {
        int[] arr1;
        int lengthOfArr1 = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] <= 3) {
                lengthOfArr1++;
            }
        }
        arr1 = new int[lengthOfArr1];
        if (arr1.length != 0) {
            int indexOfArr1 = 0;
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] <= 3) {
                    arr1[indexOfArr1] = arr[i];
                    indexOfArr1++;
                }
            }
        }
        return arr1;
    }

    // Bubble sort
    private static int[] getBubbleSort(int[] arr) {

        boolean sorted = false;
        int temp;
        while(!sorted) {
            sorted = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i+1]) {
                    temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                    sorted = false;
                }
            }
        }

        /*for (int i = 1; i < arr.length; i++) {
            System.out.println("------------ " + i + " ------------");
            for (int j = 0; j < arr.length - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int value = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = value;
                }
                System.out.print(arr[j] + " ");
            }
            System.out.println();
        }*/
        return arr;
    }

    // Shaker sort
    private static int[] getSortedArrayUsingShakerSort(int[] arr) {
        int item = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - item; j++) {
                if (arr[j] > arr[j + 1]) {
                    int value = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = value;
                }
                System.out.print("-->" + arr[j] + " ");
            }
            System.out.println();
            item++;
            for (int j = arr.length - item; j > i; j--) {
                if (arr[j] < arr[j - 1]) {
                    int value = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = value;
                }
                System.out.print("<--" + arr[j] + " ");
            }
            System.out.println();
        }
        return arr;
    }
}

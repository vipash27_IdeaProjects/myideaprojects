import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {

    private final String url = "jdbc:mysql://localhost:3306/test";
    private final String user = "root";
    private final String password = "root";

    private Connection connection = null;

    private void getConnection() {
        try {
            // Загружаем драйвер
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Драйвер подключен!");
            // Создаём соединение
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("Соединение установлено!");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }


}

import java.io.File;
import java.util.List;

public class ProgramFile {

    private File[] rootDisks;

    private List<File> fileList;

    public File[] getRootDisks() {
        rootDisks = File.listRoots();
        return rootDisks;
    }
}

package data;

public class ArrayRandomData implements RandomData {

    private int lengthOfArray;
    private int[] array;

    public ArrayRandomData(int length) {
        lengthOfArray = length;
    }

    @Override
    public int[] getArrayOfRandomValues() {
        array = new int[lengthOfArray];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        return array;
    }

}

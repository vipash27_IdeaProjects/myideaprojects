package data;

public interface RandomData {

    int[] getArrayOfRandomValues();

}

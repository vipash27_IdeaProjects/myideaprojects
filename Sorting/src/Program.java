import data.ArrayRandomData;
import data.RandomData;
import sorting.BubbleSortArray;
import sorting.ShakerSortArray;
import sorting.SortingArray;

public class Program {

    private static int[] array;

    public static void main(String[] args) {

        RandomData randomData = new ArrayRandomData(7);
        array = randomData.getArrayOfRandomValues();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("\n======================================");

        SortingArray sorting = new ShakerSortArray();
        array = sorting.sortArrayOfIntegersInAscendingOrder(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

}

package sorting;

public class ShakerSortArray implements SortingArray {

    @Override
    public int[] sortArrayOfIntegersInAscendingOrder(int[] array) {
        int temp = 1;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = temp - 1; j < array.length - temp; j++) {
                if (array[j] > array[j + 1]) {
                    int box = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = box;
                }
            }
            for (int j = ((array.length - temp) - 1); j > temp - 1; j--) {
                if (array[j] < array[j - 1]) {
                    int box = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = box;
                }
            }
            temp++;
            i++;
        }
        /*int begin = 0;
        int end = 1;
        for (int i = 0; i < array.length - 1; i++) {
            System.out.println("---------- " + (i + 1) + " ----------");
            if (i % 2 == 0) {
                for (int j = begin; j < array.length - end; j++) {
                    if (array[j] > array[j + 1]) {
                        int box = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = box;
                    }
                }
                end++;
            }
            else {
                for (int j = array.length - end; j > begin; j--) {
                    if (array[j] < array[j - 1]) {
                        int box = array[j];
                        array[j] = array[j -1];
                        array[j - 1] = box;
                    }
                }
                begin++;
            }
        }*/
        return array;
    }

    @Override
    public int[] sortArrayOfIntegersInDescendingOrder(int[] array) {
        return array;
    }
}

package sorting;

public interface SortingArray {

    int[] sortArrayOfIntegersInAscendingOrder(int[] array);

    int[] sortArrayOfIntegersInDescendingOrder(int[] array);

}

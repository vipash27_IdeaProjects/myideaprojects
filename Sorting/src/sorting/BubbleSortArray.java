package sorting;

public class BubbleSortArray implements SortingArray {

    @Override
    public int[] sortArrayOfIntegersInAscendingOrder(int[] array) {
        int temp = 1;
        for (int i = 0; i < array.length - 1; i++) {
            System.out.println("---------- " + (i + 1) + " ----------");
            for (int j = 0; j < array.length - temp; j++) {
                if (array[j] > array[j + 1]) {
                    int box = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = box;
                }
                for (int k = 0; k < array.length; k++) {
                    System.out.print(array[k] + " ");
                }
                System.out.println();
            }

            temp++;
        }

        return array;
    }

    @Override
    public int[] sortArrayOfIntegersInDescendingOrder(int[] array) {
        int temp = 1;
        for (int i = 0; i < array.length - 1; i++) {
            System.out.println("---------- " + temp + " ----------");
            for (int j = 0; j < array.length - temp; j++) {
                if (array[j] < array[j + 1]) {
                    int box = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = box;
                }
                for (int k = 0; k < array.length; k++) {
                    System.out.print(array[k] + " ");
                }
                System.out.println();
            }
            temp++;
        }
        return array;
    }

}
